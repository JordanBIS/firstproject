package com.mycompany.mavenproject1;

public class Calculator {

   //Calculate VAT on an item at 23%
   public static double calculateStandardVAT(double price) {
   
      double x = Math.round(price * 0.23);
      return x;
   
   }
   
   //Calculate VAT on a meal at 13.5%
   public static double calculateMealVAT(double price) {
   
      return price * 0.135;
   
   }
   
   //Add three numbers
   public static int addThree(int a, int b, int c) {
   
      return a + b + c;
      
   }
   
   //Multiply three numbers
   public static int multiplyThree(int a, int b, int c) {
   
      return a * c  * b;
      
   }
   
   //Get max of three numbers
   public static int returnMax(int a, int b, int c) {
   
      if ((a > b) && (a > c)){
          return a;
      }else if (b > c){
          return b;
      }else if (c > a){
          return c;
      }
      return 0;
   }
}